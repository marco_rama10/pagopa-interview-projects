const Ex2 = require('./index');
const Ex2Client = require('./index.client');
const assert = require('assert').strict;


// start server
let ex2 = new Ex2();
ex2.init();


describe('chat server', function() {
    

    it('count messages', function(done) {
        // instance 2 client and send message
        let client1 = new Ex2Client();
        let client2 = new Ex2Client();

        new Promise(async resolve => {
            await client1.connect();
            await client2.connect();
            resolve();
        }).then(() => {
            client1.sendMessage('test');
            
            this.timeout(1000);

            setTimeout(() => {
                assert.equal(client2.receivedData.length, 1);
                client1.closeConnection();
                client2.closeConnection();
                done();
            }, 200);
        });
    });

    it('count messages sender', function(done) {
        // instance 2 client and send message
        let client1 = new Ex2Client();
        let client2 = new Ex2Client();

        new Promise(async resolve => {
            await client1.connect();
            await client2.connect();
            resolve();
        }).then(() => {
            client1.sendMessage('test');
        
            
            this.timeout(1000);

            setTimeout(() => {
                assert.equal(client1.receivedData.length, 0);
                client1.closeConnection();
                client2.closeConnection();
                done();
            }, 200);
        });

    });

    it('count messages multiple clients', function(done) {
        // instance 3 client and send message
        let client1 = new Ex2Client();
        let client2 = new Ex2Client();
        let client3 = new Ex2Client();

        new Promise(async resolve => {
            await client1.connect();
            await client2.connect();
            await client3.connect();
            resolve();
        }).then(() => {
            client1.sendMessage('test');
            
            
            this.timeout(1000);

            setTimeout(() => {
                assert.equal(client2.receivedData.length + client3.receivedData.length, 2);
                client1.closeConnection();
                client2.closeConnection();
                client3.closeConnection();
                done();
            }, 200);
        });

    });


    it('message content', function(done) {
        // instance 2 client and send message
        let client1 = new Ex2Client();
        let client2 = new Ex2Client();

        new Promise(async resolve => {
            await client1.connect();
            await client2.connect();
            resolve();
        }).then(() => {

            client1.sendMessage('test');
            
            this.timeout(1000);

            setTimeout(() => {
                assert.equal(client2.receivedData[0], 'test');
                client1.closeConnection();
                client2.closeConnection();
                done();
                
            }, 200);
        });

    });

    it('message content for all', function(done) {
        // instance 3 client and send message
        let client1 = new Ex2Client();
        let client2 = new Ex2Client();
        let client3 = new Ex2Client();

        new Promise(async resolve => {
            await client1.connect();
            await client2.connect();
            await client3.connect();
            resolve();
        }).then(() => {

            setTimeout(() => {
                client1.sendMessage('Hello i\'m the Client1');
            }, 100)

            setTimeout(() => {
                client2.sendMessage('Hello i\'m the Client2');
            }, 200)

            setTimeout(() => {
                client3.sendMessage('Hello i\'m the Client3');
            }, 300);

            this.timeout(2000);

            setTimeout(() => {
                assert.equal(client1.receivedData.length, 2);
                assert.equal(client2.receivedData.length, 2);
                assert.equal(client3.receivedData.length, 2);

                assert.equal(client1.receivedData[0], 'Hello i\'m the Client2');
                assert.equal(client1.receivedData[1], 'Hello i\'m the Client3');

                assert.equal(client2.receivedData[0], 'Hello i\'m the Client1');
                assert.equal(client2.receivedData[1], 'Hello i\'m the Client3');

                assert.equal(client3.receivedData[0], 'Hello i\'m the Client1');
                assert.equal(client3.receivedData[1], 'Hello i\'m the Client2');

                client1.closeConnection();
                client2.closeConnection();
                client3.closeConnection();
                done();
                
            }, 1000);
        });

    });

    it('close connection', function() {
        assert.equal(ex2.clients.length, 0);
        ex2.closeConnection();
    });

    
});
