var net = require('net');

class Ex2Client {

    client;
    receivedData;

    constructor() {
        this.client = new net.Socket();
        this.receivedData = [];
    }

    async connect() {
        return new Promise(resolve => {
            this.client.connect(10000, '127.0.0.1', function() {
                resolve();
            });

            this.client.on('data', (data) => {
                this.recMessage(data.toString());
            });
            
            this.client.on('close', () => {
                // console.log('Connection closed');
            });            
        }).then();
    }

    sendMessage(message) {
        this.client.write(message)
    }

    closeConnection() {
        this.client.destroy();
    }

    recMessage(message) {
        this.receivedData.push(message);
    }

}

module.exports = Ex2Client;